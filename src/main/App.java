/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package main;

/**
 *
 * @author Nguyen Duc Tri
 */
import main.java.View.DashBoardView;
import main.java.View.LoginView;
import main.java.controller.DashBoardController;
import main.java.controller.LoginController;
import java.io.*;
import java.awt.EventQueue;
public class App {
    public static void main(String[] args) {

        EventQueue.invokeLater(new Runnable() {
            public void run() {

                LoginView view = new LoginView();
                LoginController controller = new LoginController(view);
                // hiển thị màn hình login
                controller.showLoginView();
//                DashBoardView view = new DashBoardView();
//                DashBoardController controller = new DashBoardController(view);
//                controller.showDashView();
            }
        });
    }
}
