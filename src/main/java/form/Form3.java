/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package main.java.form;
import main.java.entity.Crime;
import java.awt.Color;
import java.util.List;
import main.java.swing.EventCallBack;
import main.java.swing.EventTextField;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
/**
 *
 * @author Nguyen Duc Tri
 */
public class Form3 extends javax.swing.JPanel {
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel lb1;

    private main.java.swing.TextFieldAnimation txt1;


    public Form3() {
        initComponents();
//        txt1.addEvent(new EventTextField() {
//            @Override
//            public void onPressed(EventCallBack call) {
//                //  Test
//                try {
//                    for (int i = 1; i <= 100; i++) {
//                        lb1.setText("Test " + i);
//                        //Thread.sleep(10);
//                    }
//                    call.done();
//                } catch (Exception e) {
//                    System.err.println(e);
//                }
//            }
//
//            @Override
//            public void onCancel() {
//
//            }
//        });

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        txtSearch = new main.java.swing.TextFieldAnimation();
        editButton = new main.java.swing.ButtonForm();
        deleteButton = new main.java.swing.ButtonForm();
        jScrollPane1 = new javax.swing.JScrollPane();
        tableCrime = new main.java.swing.TableColumn();
        boxChoose = new main.java.swing.Combobox();

        jLabel2.setFont(new java.awt.Font("SansSerif", 1, 22)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(102, 0, 0));
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("DANH SÁCH PHẠM NHÂN");

        txtSearch.setAnimationColor(new java.awt.Color(204, 153, 0));
        txtSearch.setSelectionColor(new java.awt.Color(204, 204, 0));

        editButton.setBackground(new java.awt.Color(204, 153, 0));
        editButton.setForeground(new java.awt.Color(255, 255, 255));
        editButton.setText("edit");

        deleteButton.setBackground(new java.awt.Color(255, 102, 0));
        deleteButton.setForeground(new java.awt.Color(255, 255, 255));
        deleteButton.setText("delete");

        tableCrime.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null},
                {null, null, null, null, null, null}
            },
            new String [] {
                "ID", "Tên", "Ngày sinh", "Giới tính", "Số năm tù", "Phòng"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Integer.class, java.lang.Integer.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jScrollPane1.setViewportView(tableCrime);
        if (tableCrime.getColumnModel().getColumnCount() > 0) {
            tableCrime.getColumnModel().getColumn(2).setResizable(false);
        }

        boxChoose.setLabeText("Chọn trường");
        boxChoose.setLineColor(new java.awt.Color(204, 153, 0));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(237, Short.MAX_VALUE)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 494, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(173, 173, 173))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(40, 40, 40)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(txtSearch, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(18, 18, 18)
                        .addComponent(boxChoose, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(editButton, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(deleteButton, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(60, 60, 60))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(37, 37, 37)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(editButton, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(deleteButton, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(41, 41, 41)
                        .addComponent(boxChoose, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(46, 46, 46)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 370, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(96, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents
 public void showMessage(String message) {
        JOptionPane.showMessageDialog(this, message);
    }
    public void showListCrimes(List<Crime> list) {
        int size = list.size();
        // với bảng studentTable có 5 cột, 
        // khởi tạo mảng 2 chiều crimes, trong đó:
        // số hàng: là kích thước của list student 
        // số cột: là 5
        Object [][] crimes = new Object[size][6];
        for (int i = 0; i < size; i++) {
            crimes[i][0] = list.get(i).getIDInmate();
            crimes[i][1] = list.get(i).getName();
            crimes[i][2] = list.get(i).getBirth();
            crimes[i][3] = list.get(i).getGender();
            crimes[i][4] = list.get(i).getNumOfPrison();
            crimes[i][5] = list.get(i).getRoomNumber();
        }
        tableCrime.setModel(new DefaultTableModel(crimes, columnNames));
    }
    private final String [] columnNames = new String [] {
            "ID", "Tên", "Ngày sinh", "Giới tính", "Số năm tù","Phòng"};
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private main.java.swing.Combobox boxChoose;
    private main.java.swing.ButtonForm deleteButton;
    private main.java.swing.ButtonForm editButton;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private main.java.swing.TableColumn tableCrime;
    private main.java.swing.TextFieldAnimation txtSearch;
    // End of variables declaration//GEN-END:variables
}
