package main.java.View;

import main.java.component.Menu;
import main.java.controller.Form3Controller;
import main.java.controller.LoginController;
import main.java.event.EventMenuSelected;
import main.java.form.*;
import main.java.model.ModelMenu;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import net.miginfocom.swing.MigLayout;
import org.jdesktop.animation.timing.Animator;
import org.jdesktop.animation.timing.TimingTarget;
import org.jdesktop.animation.timing.TimingTargetAdapter;

public class DashBoardView extends javax.swing.JFrame {

    private Menu menu = new Menu();
    private JPanel main = new JPanel();
    private MigLayout layout;
    private Animator animator;
    private boolean menuShow;

    public DashBoardView() {
        initComponents();
        init();
    }

    private void init() {
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        layout = new MigLayout("fill", "0[]10[]5", "0[fill]0");
        body.setLayout(layout);
        main.setOpaque(false);
        main.setLayout(new BorderLayout());
        menu.addEventMenu(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                if (!animator.isRunning()) {
                    animator.start();
                }
            }
        });
        menu.setEvent(new EventMenuSelected() {
            @Override
            public void selected(int index) {
                if (index == 0) {
                    showForm(new Form1());
                } else if (index == 1) {
                    showForm(new Form2());
                } else if (index==2) {
                    Form3 form3 = new Form3();
                    Form3Controller controller = new Form3Controller(form3);
                    controller.showForm3();
                    showForm(form3);
                } else if (index==3) {
                    showForm(new Form4());
                } else if (index==4) {
                    showForm(new Form5());
                } else if (index==5){
                    showForm(new Form6());
                }
            }
        });

        menu.addMenu(new ModelMenu("General", new ImageIcon(getClass().getResource("/main/java/icon/home.png"))));
        menu.addMenu(new ModelMenu("Add Prisoner", new ImageIcon(getClass().getResource("/main/java/icon/add.png"))));
        menu.addMenu(new ModelMenu("Manage Prisoners", new ImageIcon(getClass().getResource("/main/java/icon/managecrime.png"))));
        menu.addMenu(new ModelMenu("Add Visit Detail", new ImageIcon(getClass().getResource("/main/java/icon/addlist.png"))));
        menu.addMenu(new ModelMenu("Manage Visit Detail", new ImageIcon(getClass().getResource("/main/java/icon/managedetail.png"))));
        menu.addMenu(new ModelMenu("Import/Export", new ImageIcon(getClass().getResource("/main/java/icon/export.png"))));
        body.add(menu, "w 50!");
        body.add(main, "w 100%");
        TimingTarget target = new TimingTargetAdapter() {
            @Override
            public void timingEvent(float fraction) {
                double width;
                if (menuShow) {
                    width = 50 + (150 * (1f - fraction));
                    menu.setAlpha(1f - fraction);
                } else {
                    width = 50 + (150 * fraction);
                    menu.setAlpha(fraction);
                }
                layout.setComponentConstraints(menu, "w " + width + "!");
                body.revalidate();
            }

            @Override
            public void end() {
                menuShow = !menuShow;
            }
        };
        animator = new Animator(400, target);
        animator.setResolution(0);
        animator.setAcceleration(0.5f);
        animator.setDeceleration(0.5f);
        showForm(new Form1());
    }

    private void showForm(Component com) {
        main.removeAll();
        main.add(com);
        main.repaint();
        main.revalidate();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        body = new JPanel();


        setUndecorated(true);

        body.setBackground(new java.awt.Color(245, 245, 245));

        javax.swing.GroupLayout bodyLayout = new javax.swing.GroupLayout(body);
        body.setLayout(bodyLayout);
        bodyLayout.setHorizontalGroup(
                bodyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 1131, Short.MAX_VALUE)
        );
        bodyLayout.setVerticalGroup(
                bodyLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGap(0, 653, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(body, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(body, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents
    public Menu getMenu(){
        return this.menu;
    }
    public void addExitListener(ActionListener listener) {
        menu.addEventLogout(listener);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private JPanel body;
    // End of variables declaration//GEN-END:variables
}
