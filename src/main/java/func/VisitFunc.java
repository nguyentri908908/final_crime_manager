package main.java.func;
import main.java.entity.RegisterToVisit;
import main.java.entity.Prison;
import main.java.entity.RegisterToVisitXML;
import main.java.utils.FileUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class VisitFunc {
    private static final String RegisterToVisit_FILE_NAME = "RegisterToVisit.xml";
    private List<RegisterToVisit> listRegisterToVisits = this.readListRegisterToVisits();

    public VisitFunc() {
    }

    public void writeListRegisterToVisits(List<RegisterToVisit> RegisterToVisits) {
        RegisterToVisitXML xml = new RegisterToVisitXML();
        xml.setRegisterToVisit(RegisterToVisits);
        FileUtils.writeXMLtoFile("RegisterToVisit.xml", xml);
    }

    public List<RegisterToVisit> readListRegisterToVisits() {
        List<RegisterToVisit> list = new ArrayList();
        RegisterToVisitXML xml = (RegisterToVisitXML) FileUtils.readXMLFile("RegisterToVisit.xml", RegisterToVisitXML.class);
        if (xml != null) {
            list = xml.getRegisterToVisit();
        }

        return (List)list;
    }

    public void add(RegisterToVisit RegisterToVisit) {
        this.listRegisterToVisits.add(RegisterToVisit);
        this.writeListRegisterToVisits(this.listRegisterToVisits);
    }

    public void edit(RegisterToVisit RegisterToVisit) {
        int size = this.listRegisterToVisits.size();
        for(int i = 0; i < size; ++i) {
            if (((RegisterToVisit)this.listRegisterToVisits.get(i)).getId() == RegisterToVisit.getId()) {
                ((RegisterToVisit)this.listRegisterToVisits.get(i)).setNameOfVisiter(RegisterToVisit.getNameOfVisiter());
                ((RegisterToVisit)this.listRegisterToVisits.get(i)).setVisitLocation(RegisterToVisit.getVisitLocation());
                ((RegisterToVisit)this.listRegisterToVisits.get(i)).setPrisoner(RegisterToVisit.getPrisoner());
                ((RegisterToVisit)this.listRegisterToVisits.get(i)).setVisitStartTime(RegisterToVisit.getVisitStartTime());
                ((RegisterToVisit)this.listRegisterToVisits.get(i)).setVisitEndTime(RegisterToVisit.getVisitEndTime());
                ((RegisterToVisit)this.listRegisterToVisits.get(i)).setRelasionship(RegisterToVisit.getRelasionship());

                this.writeListRegisterToVisits(this.listRegisterToVisits);
                break;
            }
        }

    }

    public boolean delete(RegisterToVisit RegisterToVisit) {
        boolean isFound = false;
        int size = this.listRegisterToVisits.size();

        for(int i = 0; i < size; ++i) {
            if (((RegisterToVisit)this.listRegisterToVisits.get(i)).getId() == RegisterToVisit.getId()) {
                RegisterToVisit = (RegisterToVisit)this.listRegisterToVisits.get(i);
                isFound = true;
                break;
            }
        }

        if (isFound) {
            this.listRegisterToVisits.remove(RegisterToVisit);
            this.writeListRegisterToVisits(this.listRegisterToVisits);
            return true;
        } else {
            return false;
        }
    }

    public void sortRegisterToVisitByDate() {
        Collections.sort(this.listRegisterToVisits, new Comparator<RegisterToVisit>() {
            public int compare(RegisterToVisit RegisterToVisit1, RegisterToVisit RegisterToVisit2) {
                int cmp = (RegisterToVisit1.getVisitStartTime().getYear() - RegisterToVisit2.getVisitStartTime().getYear());
                if (cmp == 0) {
                    cmp = (RegisterToVisit1.getVisitStartTime().getMonthValue() - RegisterToVisit2.getVisitStartTime().getMonthValue());
                    if (cmp == 0) {
                        cmp = (RegisterToVisit1.getVisitStartTime().getDayOfMonth() - RegisterToVisit2.getVisitStartTime().getDayOfMonth());
                    }
                }
                return cmp;
            }
        });
    }

    public List<RegisterToVisit> getListRegisterToVisits() {
        return this.listRegisterToVisits;
    }

    public void setListRegisterToVisits(List<RegisterToVisit> listRegisterToVisits) {
        this.listRegisterToVisits = listRegisterToVisits;
    }
}

