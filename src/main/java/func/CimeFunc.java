package main.java.func;
import main.java.entity.Crime;
import main.java.entity.Prison;
import main.java.utils.FileUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class CimeFunc {
    private static final String Crime_FILE_NAME = "crime.xml";
    private List<Crime> listCrimes = this.readListCrimes();

    public CimeFunc() {
    }

    public void writeListCrimes(List<Crime> Crimes) {
        Prison prison = new Prison();
        prison.setCrime(Crimes);
        FileUtils.writeXMLtoFile(Crime_FILE_NAME, prison);
    }

    public List<Crime> readListCrimes() {
        List<Crime> list = new ArrayList();
        Prison prison = (Prison)FileUtils.readXMLFile(Crime_FILE_NAME, Prison.class);
        if (prison != null) {
            list = prison.getCrime();
        }

        return (List)list;
    }

    public void add(Crime Crime) {
        this.listCrimes.add(Crime);
        this.writeListCrimes(this.listCrimes);
    }

    public void edit(Crime Crime) {
        int size = this.listCrimes.size();
        for(int i = 0; i < size; ++i) {
            if (((Crime)this.listCrimes.get(i)).getIDInmate() == Crime.getIDInmate()) {
                ((Crime)this.listCrimes.get(i)).setName(Crime.getName());
                ((Crime)this.listCrimes.get(i)).setBirth(Crime.getBirth());
                ((Crime)this.listCrimes.get(i)).setGender(Crime.getGender());
                ((Crime)this.listCrimes.get(i)).setGuilty(Crime.getGuilty());
                ((Crime)this.listCrimes.get(i)).setRoomNumber(Crime.getRoomNumber());
                ((Crime)this.listCrimes.get(i)).setNumOfPrison(Crime.getNumOfPrison());
            
                this.writeListCrimes(this.listCrimes);
                break;
            }
        }

    }

    public boolean delete(Crime Crime) {
        boolean isFound = false;
        int size = this.listCrimes.size();

        for(int i = 0; i < size; ++i) {
            if (((Crime)this.listCrimes.get(i)).getIDInmate() == Crime.getIDInmate()) {
                Crime = (Crime)this.listCrimes.get(i);
                isFound = true;
                break;
            }
        }

        if (isFound) {
            this.listCrimes.remove(Crime);
            this.writeListCrimes(this.listCrimes);
            return true;
        } else {
            return false;
        }
    }

    public void sortCrimeByName() {
        Collections.sort(this.listCrimes, new Comparator<Crime>() {
            public int compare(Crime Crime1, Crime Crime2) {
                return Crime1.getName().compareTo(Crime2.getName());
            }
        });
    }

    public void sortCrimeByYearInPrison() {
        Collections.sort(this.listCrimes, new Comparator<Crime>() {
            public int compare(Crime Crime1, Crime Crime2) {
                return Crime1.getNumOfPrison() > Crime2.getNumOfPrison() ? 1 : -1;
            }
        });
    }

    public List<Crime> getListCrimes() {
        return this.listCrimes;
    }

    public void setListCrimes(List<Crime> listCrimes) {
        this.listCrimes = listCrimes;
    }
}

