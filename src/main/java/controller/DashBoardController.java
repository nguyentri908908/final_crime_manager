/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package main.java.controller;
import main.java.View.DashBoardView;
import main.java.View.LoginView;
import main.java.entity.User;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 *
 * @author Nguyen Duc Tri
 */
public class DashBoardController {
    private DashBoardView dashView;
    public DashBoardController(DashBoardView dashView){
    this.dashView=dashView;
    dashView.addExitListener(new ExitListener());
    }
    public void showDashView() {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(DashBoardView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(DashBoardView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(DashBoardView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(DashBoardView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        dashView.setVisible(true);
        
    }
    class ExitListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            LoginView view =  new LoginView();
            LoginController controller = new LoginController(view);
            controller.showLoginView();
            dashView.setVisible(false);
        }
    }
    
}
