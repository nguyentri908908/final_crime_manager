package main.java.controller;

import main.java.entity.Crime;
import main.java.form.Form3;
import main.java.func.CimeFunc;

import java.util.List;

public class Form3Controller {
    private CimeFunc crimeFunc;
    private Form3 view;
    public Form3Controller(Form3 view){
        this.view=view;
        crimeFunc = new CimeFunc();
    }
    public void showForm3(){
        List<Crime> crimeList = crimeFunc.getListCrimes();
        view.showListCrimes(crimeList);
    }
}
