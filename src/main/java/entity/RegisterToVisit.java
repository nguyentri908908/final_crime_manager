package main.java.entity;
import java.time.*;
public class RegisterToVisit {
    private int Id;
    private Crime prisoner;
    private LocalDateTime visitStartTime;
    private LocalDateTime visitEndTime;
    private String visitLocation;
    private String NameOfVisiter;
    private String Relasionship;

    public RegisterToVisit(int Id,Crime prisoner, LocalDateTime visitStartTime, LocalDateTime visitEndTime, String visitLocation, String NameOfVisitter,String Relasionship) {
        this.Id = Id;
        this.prisoner = prisoner;
        this.visitStartTime = visitStartTime;
        this.visitEndTime = visitEndTime;
        this.visitLocation = visitLocation;
        this.NameOfVisiter = NameOfVisitter;
        this.Relasionship = Relasionship;
    }

    // Các phương thức getter và setter cho các thuộc tính của đăng ký thăm nuôi
    public Crime getPrisoner() {
        return prisoner;
    }
    public int getId(){
        return Id;
    }
    public void setId(int Id){
        this.Id=Id;
    }
    public void setPrisoner(Crime prisoner) {
        this.prisoner = prisoner;
    }

    public LocalDateTime getVisitStartTime() {
        return visitStartTime;
    }

    public void setVisitStartTime(LocalDateTime visitStartTime) {
        this.visitStartTime = visitStartTime;
    }

    public LocalDateTime getVisitEndTime() {
        return visitEndTime;
    }

    public void setVisitEndTime(LocalDateTime visitEndTime) {
        this.visitEndTime = visitEndTime;
    }

    public String getVisitLocation() {
        return visitLocation;
    }

    public void setVisitLocation(String visitLocation) {
        this.visitLocation = visitLocation;
    }

    public String getNameOfVisiter(){
        return NameOfVisiter;
    }
    public void setNameOfVisiter(String NameOfVisitter){
        this.NameOfVisiter=NameOfVisitter;
    }
    public String getRelasionship(){
        return Relasionship;
    }
    public void setRelasionship(String Relasionship){
        this.Relasionship = Relasionship;
    }
}