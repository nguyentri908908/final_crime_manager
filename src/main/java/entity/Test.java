package main.java.entity;
import java.io.Serializable;
import javax.lang.model.element.Name;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlAttribute;
import java.time.*;
@XmlRootElement(
        name = "test"
)
@XmlAccessorType(XmlAccessType.FIELD)
public class Test implements Serializable{
    private String name;
    private String id;

    Test (String name, String id){
        this.id = id;
        this.name=name;
    }
    public void setId(String Id){
        this.id=Id;
    }
    public String getId(){
        return this.id;
    }
    public void setName(String name){
        this.name=name;
    }
    public String getName(){
        return name;
    }
}
